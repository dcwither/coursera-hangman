import GamePhrase from 'components/GamePhrase/GamePhrase'
import React from 'react'
import { shallow } from 'enzyme'

import { createGame } from '../models/Game'

describe('(Component) GamePhrase', () => {
  const game = createGame()

  it('should exist', () => {
    const wrapper = shallow(<GamePhrase game={game} />)
    expect(wrapper.is('div')).to.be.true
  })

  it('should contain the game phrase as text', () => {
    const wrapper = shallow(<GamePhrase game={game} />)
    expect(wrapper.text()).to.equal(game.phrase)
  })
})
