import GameState from 'components/GameState'
import React from 'react'
import { shallow } from 'enzyme'
import { gameStateUrl } from 'utils/GameUtils'

import { createGame } from '../models/Game'

describe('(Component) GameState', () => {
  const game = createGame()

  it('should exist', () => {
    const wrapper = shallow(<GameState game={game} />)
    expect(wrapper.is('div')).to.be.true
  })

  it('should contain "You Win!" when won', () => {
    const wonGame = {
      ...game,
      state: 'won'
    }
    const wrapper = shallow(<GameState game={wonGame} />)
    expect(wrapper.text()).to.equal('You Win!')
  })

  it('should contain "Better Luck Next Time" when lost', () => {
    const lostGame = {
      ...game,
      state: 'lost'
    }
    const wrapper = shallow(<GameState game={lostGame} />)
    expect(wrapper.text()).to.equal('Better Luck Next Time')
  })

  it('should contain image with correct url', () => {
    const wrapper = shallow(<GameState game={game} />)
    expect(wrapper.find('img').props().src).to.equal(gameStateUrl(game))
  })
})
