import _ from 'lodash'
import GameList from 'components/GameList'
import React from 'react'
import { shallow } from 'enzyme'

import { createGame } from '../models/Game'

describe('(Component) GameList', () => {
  const game = createGame()

  it('should exist', () => {
    const wrapper = shallow(<GameList games={[game]} guessGame={_.noop} deleteGame={_.noop} push={_.noop} />)
    expect(wrapper.is('List')).to.be.true
  })

  it('should have one GamePreview', () => {
    const wrapper = shallow(<GameList games={[game]} guessGame={_.noop} deleteGame={_.noop} push={_.noop} />)
    expect(wrapper.find('GamePreview')).to.have.length(1)
  })
})
