import _ from 'lodash'
import sinon from 'sinon'
import GamePreview from 'components/GamePreview'
import React from 'react'
import { shallow } from 'enzyme'

import { mountInThemeProvider } from '../helpers/RenderHelper'
import { simulateEvent } from '../helpers/SimulateHelper'
import { createGame } from '../models/Game'

describe('(Component) GamePreview', () => {
  const game = createGame()
  let _wrapper, _spies

  beforeEach(() => {
    _spies = {
      push: sinon.spy(),
      delete: sinon.spy()
    }
    _wrapper = mountInThemeProvider(
      <GamePreview game={game} deleteGame={_spies.delete} push={_spies.push} />
    )
  })

  it('should exist', () => {
    const wrapper = shallow(<GamePreview game={game} deleteGame={_.noop} push={_.noop} />)
    expect(wrapper.is('ListItem')).to.be.true
  })

  it('should trigger a push when touchTapped', () => {
    simulateEvent(_wrapper.find('span[type="button"]'), 'touchTap')
    _spies.push.should.have.been.calledWith(`/game/${game.key}`)
  })

  it('should trigger a delete when touchTapping right button', () => {
    simulateEvent(_wrapper.find('button[type="button"]'), 'touchTap')
    _spies.delete.should.have.been.calledWith(game.key)
  })
})
