import _ from 'lodash'
import CreateGame from 'components/CreateGame/CreateGame'
import React from 'react'
import { shallow } from 'enzyme'

import { mountInThemeProvider } from '../helpers/RenderHelper'
import { simulateEvent } from '../helpers/SimulateHelper'

describe('(Component) CreateGame', () => {
  let _wrapper, _spies, _email

  beforeEach(() => {
    _email = 'asdf'
    _spies = {
      createGame: sinon.spy()
    }
    _wrapper = mountInThemeProvider(
      <CreateGame email={_email} createGame={_spies.createGame} />
    )
  })

  it('should exist', () => {
    const wrapper = shallow(<CreateGame email='' createGame={_.noop} />)
    expect(wrapper.is('List')).to.be.true
  })

  it('should trigger createGame when tapped', () => {
    simulateEvent(_wrapper.find('span[type="button"]'), 'touchTap')
    _spies.createGame.should.have.been.calledWith()
  })
})
