import _ from 'lodash'
import keycode from 'keycode'
import sinon from 'sinon'
import LoginPanel from 'components/LoginPanel'
import React from 'react'
import { shallow } from 'enzyme'

import { mountInThemeProvider } from '../helpers/RenderHelper'
import { simulateEvent } from '../helpers/SimulateHelper'

describe('(Component) LoginPanel', () => {
  let _wrapper, _spies

  beforeEach(() => {
    _spies = {
      submit: sinon.spy()
    }
    _wrapper = mountInThemeProvider(
      <LoginPanel onSubmit={_spies.submit} />
    )
  })

  it('should exist', () => {
    const wrapper = shallow(<LoginPanel onSubmit={_.noop} />)
    expect(wrapper.is('Paper')).to.be.true
  })

  it('should not log in when no text is filled', () => {
    simulateEvent(_wrapper.find('button'), 'touchTap')
    _spies.submit.should.not.have.been.called
  })

  it('should log in when text is filled', () => {
    simulateEvent(_wrapper.find('input[type="text"]'), 'change', { target: { value: 'a' } })
    simulateEvent(_wrapper.find('button'), 'touchTap')
    _spies.submit.should.have.been.called
  })

  it('should log in through enter key when text is filled', () => {
    simulateEvent(_wrapper.find('input[type="text"]'), 'change', { target: { value: 'a' } })
    simulateEvent(_wrapper.find('input[type="text"]'), 'keyDown', {keyCode: keycode('enter')})
    _spies.submit.should.have.been.called
  })
})
