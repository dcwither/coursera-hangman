import _ from 'lodash'
import update from 'react-addons-update'
import GameInput from 'components/GameInput/GameInput'
import React from 'react'
import { shallow } from 'enzyme'

import { mountInThemeProvider } from '../helpers/RenderHelper'
import { simulateEvent } from '../helpers/SimulateHelper'
import { createGame } from '../models/Game'

describe('(Component) GameInput', () => {
  let _wrapper, _spies
  // guarantee for tests that theses are and aren't selected respectively
  const game = update(createGame(), {
    guesses: {$merge: {
      A: true,
      B: false
    }}
  })

  beforeEach(() => {
    _spies = {
      guessGame: sinon.spy()
    }
    _wrapper = mountInThemeProvider(
      <GameInput game={game} guessGame={_spies.guessGame} />
    )
  })

  it('should exist', () => {
    const wrapper = shallow(<GameInput game={game} guessGame={_.noop} />)
    expect(wrapper.is('div')).to.be.true
  })

  it('should have 26 letter buttons', () => {
    const wrapper = shallow(<GameInput game={game} guessGame={_.noop} />)
    expect(wrapper.find('div[data-letter]')).to.have.length(26)
  })

  it('should not trigger guessGame when innavtive key tapped', () => {
    simulateEvent(_wrapper.find('div[data-letter="A"]'), 'touchTap')
    _spies.guessGame.should.not.have.been.calledWith()
  })

  it('should trigger guessGame when active key tapped', () => {
    simulateEvent(_wrapper.find('div[data-letter="B"]'), 'touchTap')
    _spies.guessGame.should.have.been.calledWith()
  })

  // TODO: figure out how to simulate keypress in enzyme to test that it works as expected
})
