import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import React from 'react'
import { mount } from 'enzyme'

export function mountInThemeProvider (component) {
  return mount(
    <MuiThemeProvider>
      {component}
    </MuiThemeProvider>
  )
}
