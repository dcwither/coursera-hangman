import injectTapEventPlugin from 'react-tap-event-plugin'
import ReactDOM from 'react-dom'
import TestUtils from 'react-addons-test-utils'

injectTapEventPlugin()

export function simulateEvent (wrappedTarget, eventType, mock) {
  if (wrappedTarget.node) {
    // wrappedTarget was obtained using enzyme's mount()
    const domNode = ReactDOM.findDOMNode(wrappedTarget.node)
    TestUtils.Simulate[eventType](domNode, mock)
  } else {
    // wrappedTarget was obtained using enzyme's shallow()
    wrappedTarget.simulate(eventType, mock)
  }
}
