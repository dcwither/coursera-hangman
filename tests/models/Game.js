const prefix = 'jZhRaXVwckjSBvWeU4ILOLKyWxqCOx96'
let index = 0

export function createGame (key = index) {
  return {
    createdAt: '2016-07-03T12:44:49-07:00',
    guesses: {
      A: true,
      B: false,
      C: true,
      D: false,
      E: true,
      F: false,
      G: false,
      H: true,
      I: true,
      J: false,
      K: false,
      L: false,
      M: true,
      N: true,
      O: true,
      P: false,
      Q: false,
      R: true,
      S: true,
      T: true,
      U: false,
      V: false,
      W: true,
      X: false,
      Y: false,
      Z: false
    },
    key: `${prefix}${index++}`,
    phrase: 'when _o_ watch it',
    state: 'alive',
    missesRemaining: 2
  }
}
