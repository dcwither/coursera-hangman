import _ from 'lodash'
import reducer, { initialState, actions } from 'redux/modules/Messages'

describe('(Redux) Messages', () => {
  describe('(Reducer)', () => {
    const sampleMessage = {
      message: 'testing testing 123',
      severity: 'error'
    }
    const startingState = [
      {
        message: 'starting message',
        severity: 'info'
      }
    ]

    it('sets up initial state', () => {
      expect(reducer(undefined, {})).to.eql(initialState)
    })

    it('should contain message once received', () => {
      expect(
        _.first(reducer([], actions.receiveMessage(sampleMessage)))
      ).to.eql(sampleMessage)
    })

    it('should increase in size once message received', () => {
      expect(
        _.size(reducer(startingState, actions.receiveMessage(sampleMessage)))
      ).to.eql(2)
    })

    it('should remain empty once empty messages dequeued message', () => {
      expect(
        _.size(reducer([], actions.dequeueMessage()))
      ).to.eql(0)
    })

    it('should decrease in size once dequeued message', () => {
      expect(_.size(reducer(startingState, actions.dequeueMessage()))).to.eql(0)
    })

    it('should second message should remain after dequeued from first state', () => {
      expect(
        _.first(reducer([...startingState, sampleMessage], actions.dequeueMessage()))
      ).to.eql(sampleMessage)
    })
  })
})
