import _ from 'lodash'
import update from 'react-addons-update'
import reducer, { initialState, actions, receiveGame } from 'redux/modules/Games'
import { actions as authActions } from 'redux/modules/Auth'

import { createGame } from '../../models/Game'

describe('(Redux) Games', () => {
  describe('(Reducer)', () => {
    const game1 = createGame()
    const game2 = createGame()
    const games = {[game1.key]: game1}
    const otherAction = {
      type: 'OTHER_ACTION',
      game: game1
    }

    it('sets up initial state', () => {
      expect(reducer(undefined, {})).to.eql(initialState)
    })

    it('clears data when logging out', () => {
      expect(reducer(games, authActions.logout())).to.eql(initialState)
    })

    it('stays same size when receiving existing game', () => {
      expect(_.size(reducer(games, receiveGame(game1)))).to.eql(_.size(games))
    })

    it('updates game when receiving existing game', () => {
      const modifiedGame = update(game1, {guesses: {B: {$set: true}}})
      expect(
        reducer(games, receiveGame(modifiedGame))[game1.key]
      ).to.eql(modifiedGame)
    })

    it('increases in size when receiving new game', () => {
      expect(
        _.size(reducer(games, receiveGame(game2)))
      ).to.eql(_.size(games) + 1)
    })

    it('decreases in size when a present game is deleted', () => {
      expect(
        _.size(reducer(games, actions.deleteGame(game1.key)))
      ).to.eql(_.size(games) - 1)
    })

    it('remains the same when an absent game is deleted', () => {
      expect(
        _.size(reducer(games, actions.deleteGame(game2.key)))
      ).to.eql(_.size(games))
    })

    it('ignores other actions', () => {
      expect(reducer(games, otherAction)).to.eql(games)
    })
  })
})
