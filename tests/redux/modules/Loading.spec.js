import reducer, { initialState } from 'redux/modules/Loading'
import { createGame, guessGame, receiveGame, receiveFailure } from 'redux/modules/Games'

describe('(Redux) Loading', () => {
  describe('(Reducer)', () => {
    it('sets up initial state', () => {
      expect(reducer(undefined, {})).to.eql(initialState)
    })

    it('it increases when creating a game', () => {
      expect(reducer(0, createGame(''))).to.eql(1)
    })

    it('it increases when guessing a game', () => {
      expect(reducer(0, guessGame({}, 'A'))).to.eql(1)
    })

    it('it decreases when receiving a game', () => {
      expect(reducer(1, receiveGame({}))).to.eql(0)
    })

    it('it decreases when receiving a game', () => {
      expect(reducer(1, receiveFailure())).to.eql(0)
    })
  })
})
