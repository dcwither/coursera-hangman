import reducer, { initialState } from 'redux/modules/Redirect'
import { actions as authActions } from 'redux/modules/Auth'
import { receiveGame } from 'redux/modules/Games'

import { createGame } from '../../models/Game'

describe('(Redux) Redirect', () => {
  describe('(Reducer)', () => {
    const email = 'test@test.com'
    const game = createGame()
    const sampleState = {
      locationBeforeTransitions: {
        pathname: '/asdf'
      }
    }

    it('sets up initial state', () => {
      expect(reducer(undefined, {})).to.eql(initialState)
    })

    it('redirect to main page after login', () => {
      expect(
        reducer(sampleState, authActions.login(email)).locationBeforeTransitions.pathname
      ).to.eql('/')
    })

    it('redirect to login page after logout', () => {
      expect(
        reducer(sampleState, authActions.logout()).locationBeforeTransitions.pathname
      ).to.eql('/login')
    })

    it('redirect to game page receive game', () => {
      expect(
        reducer(sampleState, receiveGame(game)).locationBeforeTransitions.pathname
      ).to.eql(`/game/${game.key}`)
    })

    // don't ignore other actions because some behaviour is managed by react redux router reducer
  })
})
