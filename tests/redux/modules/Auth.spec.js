import reducer, { initialState, actions } from 'redux/modules/Auth'

describe('(Redux) Auth', () => {
  describe('(Reducer)', () => {
    const email = 'test@test.com'
    const otherAction = {
      type: 'OTHER_ACTION',
      email: email
    }
    it('sets up initial state', () => {
      expect(reducer(undefined, {})).to.eql(initialState)
    })

    it('logs in', () => {
      expect(reducer(null, actions.login(email))).to.eql(email)
    })

    it('logs out', () => {
      expect(reducer(email, actions.logout())).to.eql(null)
    })

    it('ignores other actions', () => {
      expect(reducer(email, otherAction)).to.eql(email)
    })
  })
})
