import React from 'react'
import { shallow } from 'enzyme'

import <%= pascalEntityName %> from 'components/<%= pascalEntityName %>'
import { simulateEvent } from '../helpers/SimulateHelper'

describe('(Component) <%= pascalEntityName %>', () => {
  let _wrapper

  beforeEach(() => {
    _wrapper = shallow(<<%= pascalEntityName %> />)
  })

  it('should exist', () => {
    // expect(_wrapper.is('div')).to.be.true
  })
})
