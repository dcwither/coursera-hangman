import _ from 'lodash'
import sinon from 'sinon'
import <%= pascalEntityName %> from 'containers/<%= pascalEntityName %>'
import React from 'react'
import { shallow } from 'enzyme'

import { mountInThemeProvider } from '../helpers/RenderHelper'
import { simulateEvent } from '../helpers/SimulateHelper'

describe('(Component) <%= pascalEntityName %>', () => {
  let _wrapper

  beforeEach(() => {
    _wrapper = mountInThemeProvider(null)
  })

  it('should exist', () => {
    // expect(_wrapper.find('<<%= pascalEntityName %> />').is('<<%= pascalEntityName %> />')).to.be.true
  })
})
