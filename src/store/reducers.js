import { combineReducers } from 'redux'

import email from '../redux/modules/Auth'
import games from '../redux/modules/Games'
import loading from '../redux/modules/Loading'
import messages from '../redux/modules/Messages'
import router from '../redux/modules/Redirect'

export const makeRootReducer = () => {
  return combineReducers({
    // Add sync reducers here
    router,
    email,
    games,
    loading,
    messages
  })
}

export default makeRootReducer
