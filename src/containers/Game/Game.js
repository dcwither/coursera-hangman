import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import GameInput from '../../components/GameInput'
import GamePhrase from '../../components/GamePhrase'
import GameState from '../../components/GameState'
import { GameShape } from '../../constants/Shapes'
import { actions } from '../../redux/modules/Games'

export class Game extends React.Component {

  render () {
    const { game, guessGame } = this.props
    if (game) {
      return <div style={{padding: '24px'}}>
        <GameState game={game} />
        <GamePhrase game={game} />
        <GameInput game={game} guessGame={guessGame} />
      </div>
    } else {
      return null
    }
  }
}

Game.propTypes = {
  game: GameShape,
  guessGame: React.PropTypes.func.isRequired
}

const mapStateToProps = (state, {params}) => {
  return {
    game: state.games[params.gameKey]
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game)
