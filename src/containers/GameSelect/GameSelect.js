import _ from 'lodash'
import Divider from 'material-ui/Divider'
import React from 'react'
import Subheader from 'material-ui/Subheader'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'

import CreateGame from '../../components/CreateGame'
import GameList from '../../components/GameList'
import { GameShape } from '../../constants/Shapes'
import { actions as gameActions } from '../../redux/modules/Games'

export class GameSelect extends React.Component {

  render () {
    const { createGame, deleteGame, email, games, isLoading, push } = this.props
    const wonCount = _.filter(games, {state: 'won'}).length
    const lostCount = _.filter(games, {state: 'lost'}).length
    return <div>
      <CreateGame createGame={createGame} email={email} isLoading={isLoading} />
      <Divider />
      <Subheader>Current Games</Subheader>
      <GameList deleteGame={deleteGame} games={_.filter(games, {state: 'alive'})} push={push} />
      <Divider />
      <Subheader>Finished Games - Won {wonCount}, Lost {lostCount}</Subheader>
      <GameList deleteGame={deleteGame} games={_.reject(games, {state: 'alive'})} push={push} />
    </div>
  }
}

GameSelect.propTypes = {
  createGame: React.PropTypes.func.isRequired,
  deleteGame: React.PropTypes.func.isRequired,
  email: React.PropTypes.string,
  games: React.PropTypes.arrayOf(GameShape).isRequired,
  isLoading: React.PropTypes.bool.isRequired,
  push: React.PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  return {
    email: state.email,
    games: _.map(state.games),
    isLoading: state.loading > 0
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    ...gameActions,
    push
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameSelect)
