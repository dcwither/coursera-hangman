import 'react-progress-bar-plus/lib/progress-bar.css'

import _ from 'lodash'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import ProgressBar from 'react-progress-bar-plus'
import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'

import classes from '../../styles/core.scss'
import Header from '../../components/Header'
import MessageCenter from '../../components/MessageCenter'
import { actions as authActions } from '../../redux/modules/Auth'
import { actions as messageActions } from '../../redux/modules/Messages'

export class Home extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      progress: -1
    }
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.isLoading !== nextProps.isLoading) {
      this.setState({progress: nextProps.isLoading ? 0 : 100})
    } else if (!this.props.isLoading) {
      this.setState({progress: -1})
    }
  }

  render () {
    const { children, dequeueMessage, email, logout, message, push } = this.props
    return <MuiThemeProvider>
      <div style={{height: '100%'}}>
        <ProgressBar percent={this.state.progress} autoIncrement spinner={false} />
        <Header email={email} push={push} logout={logout} />
        <div className={classes.appBody}>
          {children}
        </div>
        <MessageCenter dequeueMessage={dequeueMessage} message={message} />
      </div>
    </MuiThemeProvider>
  }
}

Home.propTypes = {
  children: React.PropTypes.element,
  dequeueMessage: React.PropTypes.func.isRequired,
  email: React.PropTypes.string,
  isLoading: React.PropTypes.bool.isRequired,
  logout: React.PropTypes.func.isRequired,
  message: MessageCenter.propTypes.message,
  push: React.PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  return {
    email: state.email,
    isLoading: state.loading > 0,
    message: _.first(state.messages)
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    ...authActions,
    ...messageActions,
    push
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
