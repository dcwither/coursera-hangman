import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import LoginPanel from '../../components/LoginPanel'
import { actions } from '../../redux/modules/Auth'

export class Login extends React.Component {
  render () {
    return <LoginPanel onSubmit={this.props.login}/>
  }
}

Login.propTypes = {
  login: React.PropTypes.func.isRequired,
  email: React.PropTypes.string
}

const mapStateToProps = (state) => {
  return {
    email: state.email
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
