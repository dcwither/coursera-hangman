import React from 'react'

export const GameShape = React.PropTypes.shape({
  createdAt: React.PropTypes.string.isRequired,
  guesses: React.PropTypes.objectOf(React.PropTypes.bool).isRequired,
  key: React.PropTypes.string.isRequired,
  missesRemaining: React.PropTypes.number.isRequired,
  phrase: React.PropTypes.string.isRequired,
  state: React.PropTypes.oneOf(['won', 'lost', 'alive']).isRequired
})
