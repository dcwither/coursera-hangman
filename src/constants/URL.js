const BASE_URL = 'https://hangman.coursera.org'

export const CREATE_GAME_URL = `${BASE_URL}/hangman/game`

export const GAME_BASE_URL = `${BASE_URL}/hangman/game/`

export const requestSettings = {
  method: 'POST',
  contentType: 'text/plain'
}
