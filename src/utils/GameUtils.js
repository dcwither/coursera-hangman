import _ from 'lodash'
import moment from 'moment'

import State0 from './assets/state-0.png'
import State1 from './assets/state-1.png'
import State2 from './assets/state-2.png'
import State3 from './assets/state-3.png'
import State4 from './assets/state-4.png'
import State5 from './assets/state-5.png'
import StateLost from './assets/state-lost.png'
import StateWonAnimated from './assets/state-won.gif'
import StateWonFrozen from './assets/state-won.png'

const initialGueses = _('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
  .keyBy()
  .mapValues(_.constant(false))
  .value()

export function updateGame (game, response) {
  const parsedGame = JSON.parse(response)
  return {
    ...game,
    key: parsedGame.game_key,
    phrase: parsedGame.phrase,
    state: parsedGame.state,
    missesRemaining: parseInt(parsedGame.num_tries_left)
  }
}

export function newGame (response) {
  return updateGame({
    createdAt: moment().format(),
    guesses: initialGueses
  }, response)
}

export function gameStateUrl (game, animated = true) {
  const missesRemainingToState = {
    '5': State0,
    '4': State1,
    '3': State2,
    '2': State3,
    '1': State4,
    '0': State5
  }

  switch (game.state) {
    case 'won':
      return animated ? StateWonAnimated : StateWonFrozen

    case 'lost':
      return StateLost
    default:
      return missesRemainingToState[game.missesRemaining]
  }
}
