// Constants
export const constants = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT'
}

function login (email) {
  return {
    type: constants.LOGIN,
    email
  }
}

function logout () {
  return {
    type: constants.LOGOUT
  }
}

// Action Creators
export const actions = {
  login,
  logout
}

// Reducer
export const initialState = null
export default function (state = initialState, action) {
  switch (action.type) {
    case constants.LOGIN:
      return action.email

    case constants.LOGOUT:
      return null

    default:
      return state
  }
}
