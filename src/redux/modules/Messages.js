import _ from 'lodash'

// Constants
export const constants = {
  DEQUEUE_MESSAGE: 'DEQUEUE_MESSAGE',
  RECEIVE_MESSAGE: 'RECEIVE_MESSAGE'
}

function receiveMessage (message) {
  return {
    type: constants.RECEIVE_MESSAGE,
    message
  }
}

function dequeueMessage () {
  return {
    type: constants.DEQUEUE_MESSAGE
  }
}

// Action Creators
export const actions = {
  receiveMessage,
  dequeueMessage
}

export const initialState = []
export default function errors (state = initialState, action) {
  switch (action.type) {
    case constants.DEQUEUE_MESSAGE:
      return _.tail(state)

    case constants.RECEIVE_MESSAGE:
      return [...state, action.message]

    default:
      return state
  }
}
