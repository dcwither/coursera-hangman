import { constants as gameConstants } from './Games'

export const initialState = 0
export default function loading (state = initialState, action) {
  switch (action.type) {
    case gameConstants.CREATE_GAME:
    case gameConstants.GUESS_GAME:
      return state + 1

    case gameConstants.RECEIVE_FAILURE:
    case gameConstants.RECEIVE_GAME:
      return state - 1

    default:
      return state
  }
}
