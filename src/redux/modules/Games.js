import _ from 'lodash'
import $ from 'jquery'
import update from 'react-addons-update'

import { requestSettings, CREATE_GAME_URL, GAME_BASE_URL } from '../../constants/URL'
import { newGame, updateGame } from '../../utils/GameUtils'
import { constants as authConstants } from './Auth'
import { actions as messageActions } from './Messages'

// Constants
export const constants = {
  CREATE_GAME: 'CREATE_GAME',
  DELETE_GAME: 'DELETE_GAME',
  GUESS_GAME: 'GUESS_GAME',
  RECEIVE_GAME: 'RECEIVE_GAME',
  RECEIVE_FAILURE: 'RECEIVE_FAILURE'
}

// expose for tests
export function createGame () {
  // Inform when creating game
  return {
    type: constants.CREATE_GAME
  }
}

// expose for tests
export function guessGame (game, guess) {
  // Inform when making a guess
  return {
    type: constants.GUESS_GAME,
    game,
    guess
  }
}

// expose for tests
export function receiveGame (game) {
  return {
    type: constants.RECEIVE_GAME,
    game
  }
}

// expose for tests
export function receiveFailure () {
  return {
    type: constants.RECEIVE_FAILURE
  }
}

function deleteGame (key) {
  return {
    type: constants.DELETE_GAME,
    key
  }
}

function createNewGameThunk (email) {
  return (dispatch) => {
    dispatch(createGame())
    $.ajax({
      ...requestSettings,
      url: CREATE_GAME_URL,
      data: JSON.stringify({email})
    })
      .then(newGame)
      .then((game) => dispatch(receiveGame(game)))
      .fail((response) => {
        dispatch(receiveFailure())
        dispatch(messageActions.receiveMessage({text: 'Failed to create new game', severity: 'error'}))
      })
  }
}

// PRE: guess is uppercase
function guessGameThunk (game, guess) {
  return (dispatch) => {
    dispatch(guessGame(game, guess))
    $.ajax({
      ...requestSettings,
      url: `${GAME_BASE_URL}${game.key}`,
      data: JSON.stringify({guess})
    })
      .then((response) => updateGame(game, response, guess))
      .then((game) => update(game, {guesses: {[guess]: {$set: true}}}))
      .then((game) => dispatch(receiveGame(game)))
      .fail((response) => {
        dispatch(receiveFailure())
        dispatch(messageActions.receiveMessage({text: 'Failed to submit guess', severity: 'error'}))
      })
  }
}

// Action Creators
export const actions = {
  createGame: createNewGameThunk,
  deleteGame,
  guessGame: guessGameThunk
}

// Reducer
export const initialState = {}
export default function (state = initialState, action) {
  switch (action.type) {
    case authConstants.LOGIN:
    case authConstants.LOGOUT:
      return initialState

    case constants.RECEIVE_GAME:
      return {
        ...state,
        [action.game.key]: action.game
      }

    case constants.DELETE_GAME:
      return _.omit(state, action.key)

    default:
      return state
  }
}
