import _ from 'lodash'
import update from 'react-addons-update'
import { push, routerReducer as reduxRouterReducer } from 'react-router-redux'

import { constants as authConstants } from './Auth'
import { constants as gameConstants } from './Games'

export function initialRedirect (store) {
  const state = store.getState()
  if (_.isEmpty(state.email)) {
    store.dispatch(push('/login'))
  } else if (state.router.locationBeforeTransitions.pathname === '/login') {
    store.dispatch(push('/'))
  }
}

export const initialState = reduxRouterReducer(undefined, {})
export default function router (state = initialState, action) {
  switch (action.type) {
    case authConstants.LOGIN:
      return update(state, {
        locationBeforeTransitions: {pathname: {$set: '/'}}
      })

    case authConstants.LOGOUT:
      return update(state, {
        locationBeforeTransitions: {pathname: {$set: '/login'}}
      })

    case gameConstants.RECEIVE_GAME:
      return update(state, {
        locationBeforeTransitions: {pathname: {$set: `/game/${action.game.key}`}}
      })

    default:
      return reduxRouterReducer(state, action)
  }
}
