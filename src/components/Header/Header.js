import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import React from 'react'
import { blueGrey900 } from 'material-ui/styles/colors'

const styles = {
  title: {
    cursor: 'pointer'
  },
  appBar: {
    backgroundColor: blueGrey900
  }
}

export class Header extends React.Component {

  handleTitleTouchTap () {
    if (this.props.email) {
      this.props.push('/')
    }
  }

  renderDropDown () {
    if (this.props.email) {
      return <IconMenu
        iconButtonElement={<IconButton style={styles.menuIcon} iconClassName='material-icons'>more_vert</IconButton>}
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}
      >
        <MenuItem onTouchTap={this.props.logout} primaryText='Sign out' />
      </IconMenu>
    }
  }

  render () {
    return <AppBar
      iconElementRight={this.renderDropDown()}
      iconElementLeft={<div />}
      onTitleTouchTap={::this.handleTitleTouchTap}
      style={styles.appBar}
      title={<span style={styles.title}>Hangman</span>}
    />
  }
}

Header.propTypes = {
  email: React.PropTypes.string,
  push: React.PropTypes.func.isRequired,
  logout: React.PropTypes.func.isRequired
}

export default Header
