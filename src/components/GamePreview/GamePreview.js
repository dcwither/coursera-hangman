import moment from 'moment'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import React from 'react'
import {ListItem} from 'material-ui/List'

import { GameShape } from '../../constants/Shapes'
import { gameStateUrl } from '../../utils/GameUtils'

const styles = {
  avatar: {
    border: '1px solid black',
    backgroundColor: 'white'
  },
  previewTitle: {
    fontFamily: 'monospace',
    letterSpacing: '2px'
  }
}

export class GamePreview extends React.Component {

  handleOpenGame () {
    this.props.push(`/game/${this.props.game.key}`)
  }

  handleDeleteGame () {
    this.props.deleteGame(this.props.game.key)
  }

  renderRightIcon () {
    return <IconButton
      onTouchTap={::this.handleDeleteGame}
      iconClassName='material-icons'
    >
      delete
    </IconButton>
  }

  render () {
    const game = this.props.game
    return <ListItem
      primaryText={<span style={styles.previewTitle}>{game.phrase}</span>}
      secondaryText={moment(game.createdAt).fromNow()}
      onTouchTap={::this.handleOpenGame}
      leftAvatar={<Avatar style={styles.avatar} src={gameStateUrl(game, false)} />}
      rightIconButton={this.renderRightIcon()}
    />
  }
}

GamePreview.propTypes = {
  deleteGame: React.PropTypes.func.isRequired,
  game: GameShape.isRequired,
  push: React.PropTypes.func.isRequired
}

export default GamePreview
