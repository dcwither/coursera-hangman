import React from 'react'
import { cyan600, red600 } from 'material-ui/styles/colors'

import { GameShape } from '../../constants/Shapes'
import { gameStateUrl } from '../../utils/GameUtils'

const styles = {
  container: {
    position: 'relative'
  },
  winCondition: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    fontSize: '40px',
    textAlign: 'center',
    width: '100%'
  },
  stateImage: {
    width: '50%',
    maxWidth: '400px',
    marginLeft: '50%',
    transform: 'translateX(-50%)'
  },
  guesses: {
    fontSize: '18px'
  }
}

export class GameState extends React.Component {

  renderWinCondition () {
    switch (this.props.game.state) {
      case 'won':
        return <div style={{...styles.winCondition, color: cyan600}}>
          You Win!
        </div>

      case 'lost':
        return <div style={{...styles.winCondition, color: red600}}>
          Better Luck Next Time
        </div>

      default:
        return null

    }
  }

  render () {
    return <div style={styles.container}>
      <img style={styles.stateImage} src={gameStateUrl(this.props.game)} />
      {this.renderWinCondition()}
    </div>
  }
}

GameState.propTypes = {
  game: GameShape.isRequired
}

export default GameState
