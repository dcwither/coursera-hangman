import FontIcon from 'material-ui/FontIcon'
import React from 'react'
import { List, ListItem } from 'material-ui/List'
import { cyan500 } from 'material-ui/styles/colors'

export class CreateGame extends React.Component {

  handleCreateGame () {
    this.props.createGame(this.props.email)
  }

  render () {
    return <List>
      <ListItem
        disabled={this.props.isLoading}
        leftIcon={<FontIcon color={cyan500} className='material-icons'>add_circle</FontIcon>}
        onTouchTap={::this.handleCreateGame}
        primaryText='Start New Game'
      />
    </List>
  }
}

CreateGame.propTypes = {
  email: React.PropTypes.string,
  createGame: React.PropTypes.func.isRequired
}

export default CreateGame
