import React from 'react'

import { GameShape } from '../../constants/Shapes'

const styles = {
  phraseText: {
    fontFamily: 'monospace',
    letterSpacing: '5px',
    fontSize: '50px',
    textAlign: 'center'
  }
}
export class GamePhrase extends React.Component {

  render () {
    return (
      <div style={styles.phraseText}>{this.props.game.phrase}</div>
    )
  }
}

GamePhrase.propTypes = {
  game: GameShape.isRequired
}

export default GamePhrase
