import _ from 'lodash'
import $ from 'jquery'
import React from 'react'
import { cyan900 } from 'material-ui/styles/colors'

import { GameShape } from '../../constants/Shapes'

const styles = {
  container: {
    textAlign: 'center',
    marginTop: '20px'
  },
  letterButton: {
    display: 'inline-block',
    width: '40px',
    height: '40px',
    margin: '4px',
    textAlign: 'center',
    fontSize: '20px',
    lineHeight: '36px',
    color: cyan900,
    border: `2px solid ${cyan900}`,
    borderRadius: '3px',
    MozUserSelect: 'none',
    WebkitUserSelect: 'none',
    msUserSelect: 'none'
  },
  selected: {
    backgroundColor: cyan900,
    color: 'white',
    cursor: 'default'
  },
  unselected: {
    cursor: 'pointer'
  },
  gameOver: {
    cursor: 'default'
  }
}

export class GameInput extends React.Component {
  constructor (props) {
    super(props)
    this.handleInput = ::this.handleInput
  }

  componentDidMount () {
    $(document).on('keypress', this.handleInput)
  }

  componentWillUnmount () {
    $(document).off('keypress', this.handleInput)
  }

  guessLetter (letter) {
    if (this.props.game.state === 'alive' && this.props.game.guesses[letter] === false) {
      this.props.guessGame(this.props.game, letter)
    }
  }

  handleInput (evt) {
    if (!evt.altKey && !evt.ctrlKey && !evt.metaKey) {
      this.guessLetter(String.fromCharCode(evt.keyCode).toUpperCase())
    }
  }

  renderGuessOptions () {
    return _.map(this.props.game.guesses, (hasGuessed, letter) => {
      const style = {
        ...styles.letterButton,
        ...(hasGuessed ? styles.selected : styles.unselected),
        ...(this.props.game.state === 'alive' ? null : styles.gameOver)
      }
      return <div
        data-letter={letter}
        key={letter}
        onTouchTap={hasGuessed ? null : () => this.guessLetter(letter)}
        style={style}
      >
        {letter}
      </div>
    })
  }

  render () {
    // don't neet to render, we are just listening to events
    return <div style={styles.container}>
      {this.renderGuessOptions()}
    </div>
  }
}

GameInput.propTypes = {
  game: GameShape.isRequired,
  guessGame: React.PropTypes.func.isRequired
}

export default GameInput
