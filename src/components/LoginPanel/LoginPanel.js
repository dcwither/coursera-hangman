import _ from 'lodash'
import keycode from 'keycode'
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'
import React from 'react'
import TextField from 'material-ui/TextField'

const styles = {
  loginPanel: {
    height: '200px',
    width: '400px',
    position: 'absolute',
    top: '20%',
    left: '50%',
    transform: 'translateX(-50%)',
    padding: '20px',
    transition: 'none'
  },
  emailField: {
    display: 'block',
    width: '100%'
  },
  submitButton: {
    float: 'right',
    marginTop: '10px'
  }
}

export class LoginPanel extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      email: ''
    }
  }

  validate () {
    return !_.isEmpty(this.state.email)
  }

  handleSubmit () {
    if (this.validate()) {
      this.props.onSubmit(this.state.email)
    }
  }

  handleChange (evt) {
    this.setState({
      email: evt.target.value
    })
  }

  handleKeyDown (evt) {
    if (keycode(evt.keyCode) === 'enter') {
      this.handleSubmit()
    }
  }

  render () {
    return <Paper style={styles.loginPanel}>
      <h3>Login</h3>
      <TextField
        hintText='Enter your email'
        style={styles.emailField}
        onChange={::this.handleChange}
        onKeyDown={::this.handleKeyDown}
      />
      <RaisedButton
        label='Submit'
        primary
        style={styles.submitButton}
        disabled={!this.validate()}
        onTouchTap={::this.handleSubmit}
      />
    </Paper>
  }
}

LoginPanel.propTypes = {
  onSubmit: React.PropTypes.func.isRequired
}

export default LoginPanel
