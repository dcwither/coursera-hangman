import React from 'react'
import Snackbar from 'material-ui/Snackbar'
import { red900 } from 'material-ui/styles/colors'

const styles = {
  info: {},
  error: {
    backgroundColor: red900
  }
}

export class MessageCenter extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      severity: 'info'
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.message) {
      this.setState({
        open: true,
        severity: nextProps.message.severity
      })
    }
  }

  handleRequestClose () {
    this.setState({
      open: false
    })
    this.props.dequeueMessage()
  }

  render () {
    const { message = {text: '', severity: 'info'} } = this.props
    return <Snackbar
      bodyStyle={message && styles[this.state.severity]}
      open={this.state.open}
      message={message.text}
      autoHideDuration={2000}
      onRequestClose={::this.handleRequestClose}
    />
  }
}

MessageCenter.propTypes = {
  dequeueMessage: React.PropTypes.func.isRequired,
  message: React.PropTypes.shape({
    text: React.PropTypes.string.isRequired,
    severity: React.PropTypes.oneOf(['info', 'error']).isRequired
  })
}

export default MessageCenter
