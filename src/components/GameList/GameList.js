import _ from 'lodash'
import React from 'react'
import { List } from 'material-ui/List'

import GamePreview from '../GamePreview'
import { GameShape } from '../../constants/Shapes'

export class GameList extends React.Component {

  renderTiles () {
    const { deleteGame, games, push } = this.props
    return _.chain(games)
      .orderBy('createdAt', 'desc')
      .map((game) =>
        <GamePreview
          key={game.key}
          deleteGame={deleteGame}
          game={game}
          push={push}
        />
      )
      .value()
  }

  render () {
    return <List>
      {this.renderTiles()}
    </List>
  }
}

GameList.propTypes = {
  deleteGame: React.PropTypes.func.isRequired,
  games: React.PropTypes.arrayOf(GameShape).isRequired,
  push: React.PropTypes.func.isRequired
}

export default GameList
