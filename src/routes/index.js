// We only need to import the modules necessary for initial render

import '../redux/modules/Redirect.js'

import React from 'react'
import { IndexRoute, Route } from 'react-router'

import Game from '../containers/Game'
import GameSelect from '../containers/GameSelect'
import Home from '../containers/Home'
import Login from '../containers/Login'

export default <Route path='/' component={Home}>
  <IndexRoute component={GameSelect} />
  <Route path='login' component={Login} />
  <Route path='game/:gameKey' component={Game} />
</Route>
